﻿namespace Calculator
{
    public class SpecificCalculator
    {
        private static int Addition = 0;

        public int Sum(int a, int b) => a + b;

        public static int Add(int a) => Addition += a;

        public int Mul(int a, int b) => a * b;
    }
}
