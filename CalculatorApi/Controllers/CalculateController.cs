﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calculator;
using CalculatorApi.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatorApi.Controllers
{
    [Route("api/calculate")]
    [ApiController]
    public class CalculateController : ControllerBase
    {
        [HttpGet("sum/{a}/{b}")]
        public async Task<ActionResult<int>> Sum(int a, int b)
        {
            SpecificCalculator calc = new SpecificCalculator();
            return await Task.FromResult<ActionResult<int>>(Ok(calc.Sum(a, b)));
        }

        [HttpGet("add/{a}")]
        public async Task<ActionResult<int>> Add(int a)
            => await Task.FromResult<ActionResult<int>>(Ok(SpecificCalculator.Add(a)));

        [HttpPost("mul")]
        public async Task<ActionResult<int>> Mul([FromBody] ItemsData itemsData)
        {
            SpecificCalculator calc = new SpecificCalculator();
            return await Task.FromResult<ActionResult<int>>(Ok(calc.Mul(itemsData.a, itemsData.b)));
        }
    }
}