using Calculator;
using System;
using Xunit;

namespace CalculatorTests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            SpecificCalculator calc = new SpecificCalculator();
            Assert.Equal(12, calc.Sum(3, 9));
        }

        [Fact]
        public void Test2()
        {
            SpecificCalculator calc = new SpecificCalculator();
            Assert.Equal(27, calc.Mul(3, 9));
        }
    }
}
